var express = require('express.io');
var mongo = require('mongoose');
var graph = require('fbgraph');

mongo.connect('mongodb://noctewebserver:aabbcc0022@ds053178.mongolab.com:53178/nocte');
var db = mongo.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {
	console.log('open')
});

var Instagram = require('instagram-node-lib');
Instagram.set('client_id', 'bdc73d6a704c43ea92066beb694f9238');
Instagram.set('client_secret', '67aad9aee0184df2b8c28b977293ed47');
Instagram.set('callback_url', 'http://nocte.co/instagram/callback');
Instagram.set('redirect_uri', 'http://nocte.co/');

// SessionStore = new MongoStore({ db: 'SessionStore' })

var passport = require('passport')
  , FacebookStrategy = require('passport-facebook').Strategy;
var adminpassport = require('passport');
// var FacebookAdminStrategy = require('passport-facebook').Strategy;
var FACEBOOK_APP_ID = "1390859814486489"
var FACEBOOK_APP_SECRET = "4e3fb993a891d7e5cd85043a814c26b9";

passport.use(new FacebookStrategy({
    clientID: FACEBOOK_APP_ID,
    clientSecret: FACEBOOK_APP_SECRET,
    callbackURL: "/auth/facebook/callback"
  },
  function(accessToken, refreshToken, profile, done) {
  		// console.log(profile)
  		// console.log(done)
  		
  		// console.log(profile);
    	// console.log(passport);

    	var options = {
		    timeout:  3000
		  , pool:     { maxSockets:  Infinity }
		  , headers:  { connection:  "keep-alive" }
		};

		graph
		  .setOptions(options)
		  .get("me/accounts?limit=100&access_token=" + accessToken, function(err, res) {
		    // console.log('pages')
		    var newProfile = profile;
		    // console.log('******FB RESPONSE******');
		    // console.log(res);
		    api.findOrCreateUser(profile, done, res)
		    
		  });		  
  }
));

/* adminpassport.use(new FacebookAdminStrategy({
    clientID: FACEBOOK_APP_ID,
    clientSecret: FACEBOOK_APP_SECRET,
    callbackURL: "/auth/facebook/callback/admin"
  },
  function(accessToken, refreshToken, profile, done) {
  		console.log('admin-callback')
  		// api.findOrCreateUser(profile, done)
  		// console.log(profile);
    	console.log(accessToken);
    	console.log('ananen')
	}  
));
*/
// var passportSocketIo = require("passport.socketio");

// var SessionStore = require("session-mongoose")(express);
// var store = new SessionStore({
//     url: "mongodb://noctewebserver:aabbcc0022@ds053178.mongolab.com:53178/nocte",
//     interval: 120000 // expiration check worker run interval in millisec (default: 60000)
// });

// adminpassport.serializeUser(function(user, done) {
//   done(null, user);
// });

// adminpassport.deserializeUser(function(obj, done) {
//   done(null, obj);
// });

passport.serializeUser(function(user, done) {
	// console.log('serializeUser')
  done(null, user);
});

passport.deserializeUser(function(obj, done) {
	// console.log('deserialize')
  done(null, obj);
});

var api = require('./controllers/api.js');
var app = express();

app.http().io();

// console.log(SessionStore);

// app.io.set('authorization', passportSocketIo.authorize({
// 	cookieParser: express.cookieParser(),
//     secret: 'adkljbasljkdbasjkd123s', //the session secret to parse the cookie
//     fail: function(data, accept) {     // *optional* callbacks on success or fail
      // console.log('za')
//       accept(null, false);             // second param takes boolean on whether or not to allow handshake
//     },
//     success: function(data, accept) {
      // console.log('za')
//       accept(null, true);

//     }
// }));

app.configure(function() {
	app.engine('html',require('ejs').renderFile);
	app.set('views',__dirname + '/public/nocte-angular/angular/');
	app.set('view engine','html');
	app.use(express.bodyParser());
	app.use(express.static(__dirname+'/public/nocte-angular/angular/'));
	app.use(express.cookieParser());
	app.use(express.session({ secret: 'adkljbasljkdbasjkd123s'}));
	app.use(passport.initialize());
	app.use(passport.session());
	app.use(express.cookieParser('adkljbasljkdbasjkd123s'));
	// app.use(express.session({ key: 'express.sid', secret: 'ajgdailsfbaisbdfliausbfilsau' }));
	app.use(app.router);
});

// app.io.sockets.on("connection", function(socket){
	// console.log('zaa')
    // console.log("user connected: ", socket.handshake.user.name);

//     //filter sockets by user...
//     var userGender = socket.handshake.user.gender, 
//         opposite = userGender === "male" ? "female" : "male";

//     passportSocketIo.filterSocketsByUser(sio, function (user) {
//       return user.gender === opposite;
//     }).forEach(function(s){
//       s.send("a " + userGender + " has arrived!");
//     });

//   });


app.io.sockets.on('connection', function (con) {
	// console.log(con.id)
});	
// app.io.route('ready', function(req) {
	// console.log('za')
    // console.log(req);
// });

app.io.route('hello', function(req) {
	// console.log('hello')
	// console.log(req)
	// console.log(express.id)
    // console.log(req.sessionStore.sessions)
});

Instagram.subscriptions.subscribe({
  object: 'tag',
  object_id: 'lollapalooza',
  aspect: 'media',
  callback_url: 'http://nocte.co/instagram/callback'
});

app.get('/instagram/callback', function(req, res){
	console.log('bitch *******')
    // var handshake =  Instagram.subscriptions.handshake(req, res);
    // console.log(handshake)
});

app.get('/', function(req, res){ res.render('index'); })

app.get('/auth/facebook', passport.authenticate('facebook', { scope: 'email', callbackURL: "/auth/facebook/callback" }) );
app.get('/auth/facebookAdmin', passport.authenticate('facebook', { scope: 'email, manage_pages' }) );

// app.get('/auth/facebook/callback', 
app.get('/auth/facebook/callback', 
	// assport.authenticate('facebook',function(req, res){
		// console.log('ananen');
	// 	// res.redirect('/');
	// })

	passport.authenticate('facebook', { successRedirect: "/",
                                    failureRedirect: '/auth/facebook' }));

app.get('/auth/facebook/callback/admin', 
	// assport.authenticate('facebook',function(req, res){
		// console.log('ananen');
	// 	// res.redirect('/');
	// })

	passport.authenticate('facebook', { successRedirect: "/clubs",
                                    failureRedirect: '/auth/facebookAdmin' }));



app.get('/api/feed', api.feed );
app.get('/api/:clubname/feed', api.clubFeedList);
app.get('/api/events/:feedid', api.feedDetails);
app.get('/api/clubs/:clubname', api.clubDetails);
app.get('/api/clubs', api.clubs);
app.get('/api/djs', api.djs);
app.post('/api/djs', api.djs);
app.get('/api/djs/:djName', api.djDetails);
app.get('/api/suggestedPages', ensureAuthenticated, api.suggestedPages);



app.get('/api/users/:fbid', ensureAuthenticated, api.findUser)

app.post('/api/userPreReg', api.preRegUser)

app.post('/api/dj', ensureAuthenticated, api.newDj)
app.get('/api/dj/:djId/events', api.djEvents);

app.post('/api/club', ensureAuthenticated, api.newClub)

app.post('/api/event', ensureAuthenticated, api.newEvent)
// app.modify('/api/event', ensureAuthenticated, api.modifyEvent)


app.get('/api/fbEvents/:fbid', ensureAuthenticated, api.getFbEvents);
// app.get('/user/actions', ensureAuthenticated, api.getUserAction);

app.post('/api/user/action', ensureAuthenticated, api.newUserAction)
// app.post('/api/action/:actionName/:targetId', ensureAuthenticated,  api.newUserAction);
app.delete('/api/action/:actionType/:targetType/:targetId', ensureAuthenticated,  api.deleteUserAction);
app.post('/api/suggest/:fbid', ensureAuthenticated, api.suggestNewUser);
app.delete('/api/suggest/:fbid', ensureAuthenticated, api.deleteSuggestedUser);



// app.delete('/api/feedbacks', ensureAuthenticated, api.deleteNocteFeedback)
app.post('/api/feedbacks', api.newNocteFeedback)
app.get('/api/feedbacks', ensureAuthenticated, api.getNocteFeedbacks)
app.post('/api/update/feedbacks', ensureAuthenticated, api.updateNocteFeedbacks)

app.delete('/api/actions', ensureAuthenticated, api.deleteNocteAction)
app.post('/api/actions', ensureAuthenticated, api.newNocteAction)
app.get('/api/actions', ensureAuthenticated, api.getNocteActions)
app.get('/api/actions/:fbid', ensureAuthenticated, api.getNocteUserActions)

app.get('/api/users', ensureAuthenticated, api.getAllNocteUsers);
app.get('/api/users/achievements/:uid', ensureAuthenticated, api.getNocteUsersAchivements);
app.get('/api/users', ensureAuthenticated, api.getAllNocteUsers);
app.delete('/api/users', ensureAuthenticated, api.deleteNocteUsers);

app.get('/api/achievements', ensureAuthenticated, api.getNocteAchievements)
app.post('/api/achievements', ensureAuthenticated, api.newNocteAchievement)

app.get('/user', ensureAuthenticated, function(req, res){
	// console.log(req.user)
	// console.log('ananeler')
	api.getUserAction(req.user, res, true)
});

app.get('/user/actions', ensureAuthenticated, api.getUserActions);
app.get('/user/gameinfo', ensureAuthenticated, api.getUserGameinfo);
// app.get('/user/likes', ensureAuthenticated, function(req, res){
// 	api.getUserLikes(req.user, res, true)
// });
// app.get('/user/follows', ensureAuthenticated, function(req, res){
// 	api.getUserFollows(req.user, res, true)
// });


app.get('/api/game/levels', ensureAuthenticated, api.getAllNocteLevels);

app.get('/login', function(req,res){ res.send('exit'); })

app.listen(80);
console.log(' = 80');

function ensureAuthenticatedAdmin(req, res, next) {
  if (req.isAuthenticated()) { return next(); }
  res.redirect('/login')
}

function ensureAuthenticated(req, res, next) {
	// console.log(req)
  if (req.isAuthenticated()) { return next(); }
  res.redirect('/login')
}
