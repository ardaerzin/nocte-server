var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

var Achievement = new Schema({
	ach_id: { type: String },
	// name: String,
	// action: String,
	// repeat: Number,
	// description: String,
	// badge: Object,
	date: Date,
})

var userAchievementSchema = new Schema({
	userid: Object,
	achievements: [Achievement]
})

userAchievementModel = mongoose.model('userAchievement', userAchievementSchema);

module.exports = mongoose.model('userAchievement', userAchievementSchema);