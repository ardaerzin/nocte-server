var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

var actionsSchema = new Schema({
	name: String,
	value: Number,
	type: String,
	msg: String
})

nocteActionModel = mongoose.model('action', actionsSchema);
module.exports = mongoose.model('action', actionsSchema);