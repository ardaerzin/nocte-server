var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

var suggestedUserSchema = new Schema({
	fbid: String,
	addedBy: Object
})

suggestedUserModel = mongoose.model('suggesteduser', suggestedUserSchema);
module.exports = mongoose.model('suggesteduser', suggestedUserSchema);