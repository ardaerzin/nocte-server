var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

var achievementSchema = new Schema({
	name: String,
	action: String,
	repeat: Number,
	description: String,
	badge: Object,
	date: Date,
})

module.exports = mongoose.model('achievement', achievementSchema);