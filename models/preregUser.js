var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

var earlyUserSchema = new Schema({
	email: String
})

earlyUserModel = mongoose.model('earlyuser', earlyUserSchema);
module.exports = mongoose.model('earlyuser', earlyUserSchema);