// The Thread model
 
var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

var venueSchema = new Schema({
	name: String,
	type: String,
	fbid: Number,
	location: Object,
	description: String
})

// console.log("zazazaza")
//console.log(threadSchema);

module.exports = mongoose.model('venue', venueSchema);