var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

var feedbackSchema = new Schema({
	userEmail: String,
	fromUrl: String,
	feedbackMsg: String,
	feedbackDate: Date,
	feedbackStatus: String
})

feedbackModel = mongoose.model('feedback', feedbackSchema);
module.exports = mongoose.model('feedback', feedbackSchema);