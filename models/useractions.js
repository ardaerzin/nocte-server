var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

// var Actions = new Schema({
// 	name: String,
// 	value: Number,
// 	target_id: String,
// 	target_type: String,
// 	date: Date
// });

var userActionSchema = new Schema({
	// user_id: Object,
	// action_type: String, 
	// target_id: Object,
	// target_type: String,	//ardanin eseri

	user_id: Object,
    action_type: String,
    target_id: { type: String },
    target_parentid: { type: String },
    action_id: { type: String },
    date: Date
})
// likeSearchModel = mongoose.model('useraction', newSchema);

actionModel = mongoose.model('useraction', userActionSchema);

module.exports = mongoose.model('useraction', userActionSchema);