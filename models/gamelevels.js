var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

var gamelevelSchema = new Schema({
	lvl: Number,
	title: String,
	max_points: Number
})

gamelevelModel = mongoose.model('gamelevel', gamelevelSchema);
module.exports = mongoose.model('gamelevel', gamelevelSchema);