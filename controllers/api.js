var Feed = require('../models/feed.js');

exports.feed = function(req, res){
  FbFeed.find(function(err,feeds) {
    res.send(feeds);
  });
}

var Post = require('../models/post.js');
var Club = require('../models/club.js');
var Dj = require('../models/djs.js');
var User = require('../models/user.js');
var Actions = require('../models/actions.js');
var UserActions = require('../models/useractions.js');
var Achivements = require('../models/achievements.js');
var UserAchivements = require('../models/userAchievements.js');
var EarlyUser = require('../models/preregUser.js');
var SuggestedUser = require('../models/suggestedUser.js');
var graph = require('fbgraph');
var FbFeed = require('../models/fbFeed.js');
var Feedback = require('../models/feedbacks.js')
var GameLevel = require('../models/gamelevels.js')

var fbOptions = {
      timeout:  3000
    , pool:     { maxSockets:  Infinity }
    , headers:  { connection:  "keep-alive" }
  };


exports.getNocteAchievements = function(req,res){
  var selectedPage = null;
  // console.log(1)
  for(var i=0; i<req.user.user.pages.length; i++){
    // console.log(2)
    if(req.user.user.pages[i].id == 550232838377740){
      selectedPage = req.user.user.pages[i];
      next();
      break;
    }
  }

  function next(){
    // console.log('za23232')
    Achivements.find(function(err,achievementList) {
      res.send({code:1,nocteAchievements:achievementList});
    })
  }
}

exports.deleteNocteAction = function(req,res){
  var selectedPage = null;
  // console.log(1)
  for(var i=0; i<req.user.user.pages.length; i++){
    // console.log(2)
    if(req.user.user.pages[i].id == 550232838377740){
      selectedPage = req.user.user.pages[i];
      next();
      break;
    }
  }

  function next(){
    Actions.findOne({_id: req.body._id}).remove(function(err, data){
      res.send({code:1})
    });
    // var newAction_mongo = new nocteActionModel(req.body);
    // newAction_mongo.save( function(error, data){
    //   console.log(data)
    // })
 // SuggestedUser.findOne({fbid: req.params.fbid}).remove(function(err, data){
  }
}

exports.deleteNocteUsers = function(req,res){
  var selectedPage = null;
  console.log(1)
  for(var i=0; i<req.user.user.pages.length; i++){
    console.log(2)
    if(req.user.user.pages[i].id == 550232838377740){
      selectedPage = req.user.user.pages[i];
      next();
      break;
    }
  }

  function next(){

    // console.log(req.body.userId)
    // if(req.body.)
    User.findOne({_id:req.body.userId}).remove(function(err, data){
      // console.log(data)
      res.send({code:1})
    });
  }
}

exports.updateNocteFeedbacks = function(req, res){
  var selectedPage = null;
  // console.log(111)
  for(var i=0; i<req.user.user.pages.length; i++){
    // console.log(222)
    if(req.user.user.pages[i].id == 550232838377740){
      selectedPage = req.user.user.pages[i];
      next();
      break;
    }
  }

  function next(){

    // console.log(req.body)
    Feedback.findOne({_id:req.body.feedbackId}, function(err,nocteFeedback) {
      console.log(err + " / " + nocteFeedback)
      if(nocteFeedback){
        Feedback.update({_id: nocteFeedback._id}, {feedbackStatus: req.body.feedbackStatus}, {}, function(err, numAffected){
          // console.log(numAffected)
          if(numAffected){
            res.send({code:1, newStatus: req.body.feedbackStatus})
          }
        });

      }
    });
  }
}

exports.getNocteFeedbacks = function(req, res){
  var selectedPage = null;
  // console.log(1)
  for(var i=0; i<req.user.user.pages.length; i++){
    // console.log(2)
    if(req.user.user.pages[i].id == 550232838377740){
      selectedPage = req.user.user.pages[i];
      next();
      break;
    }
  }

  function next(){
    Feedback.find(function(err,nocteFeedbackList) {
      // console.log(err + " / " + nocteFeedbackList)
      if(nocteFeedbackList){
        res.send({code:1,nocteFeedbacks:nocteFeedbackList});
      } else {
        res.send({code:0});
      }
    })
  }
}

exports.getAllNocteUsers = function(req, res){
  var selectedPage = null;
  // console.log(1)
  for(var i=0; i<req.user.user.pages.length; i++){
    // console.log(2)
    if(req.user.user.pages[i].id == 550232838377740){
      selectedPage = req.user.user.pages[i];
      next();
      break;
    }
  }

  function next(){
    User.find(function(err,nocteUserList) {
      // console.log(err + " / " + nocteUserList)
      res.send({code:1,nocteUsers:nocteUserList});
    })
  }
}

exports.newNocteFeedback = function(req,res){
  // console.log('feedbackten user')
  // console.log(req.user)
  if(req.body.feedbackMsg){
    var newFeedback_mongo = new feedbackModel(req.body);
    newFeedback_mongo.save( function(error, data){
      if(data){
        if(req.user){
          User.findOne({'user.id':req.user.user.id}, function(err, loggedinUser) {
            addAction('feedback', '', res, loggedinUser)
          });
          // addAction('feedback', '', res, loggedinUser)
        } else {
          res.send({code:1, newActionData: data})
        }
        
      } else {
        res.send({code:0})
      }
      
    })
  }
}

exports.newNocteAchievement = function(req,res){

  var selectedPage = null;
  // console.log(1)
  for(var i=0; i<req.user.user.pages.length; i++){
    // console.log(2)
    if(req.user.user.pages[i].id == 550232838377740){
      selectedPage = req.user.user.pages[i];
      next();
      break;
    }
  }

  function next(){
    // console.log("**************")
    // console.log(req.body)
    Achivements.findOne({_id: req.body._id},function(err,data){
      if(data){
        //edit achievement
        // console.log('edit achievement')
        // console.log(req.body.badge)
        // console.log(JSON.parse(req.body.badge))
        Achivements.update({_id: data._id}, {name: req.body.name, action: req.body.action, repeat: req.body.repeat, description: req.body.description, badge: JSON.parse(req.body.badge)}, {}, function(err, numAffected){
          // console.log(numAffected)
        }); 
      } else {
        //create achievement
      }
      res.send({code:1})
    });
  }
}

exports.newNocteAction = function(req,res){
  var selectedPage = null;
  // console.log(1)
  for(var i=0; i<req.user.user.pages.length; i++){
    // console.log(2)
    if(req.user.user.pages[i].id == 550232838377740){
      selectedPage = req.user.user.pages[i];
      next();
      break;
    }
  }

  function next(){
    // console.log('za1111')
    // console.log(req.body)

    Actions.findOne({name: req.body.name},function(err,data){
      // console.log('')
      if(data){
        // console.log('edit action')
        Actions.update({_id: data._id}, {value: req.body.value, type: req.body.type, msg: req.body.msg}, {}, function(err, numAffected){
          // console.log(numAffected + " / ")
          if(numAffected > 0){
            res.send({code:1, newActionData:req.body})
          }
        });
      } else {
        // console.log('create action')
        var newAction_mongo = new nocteActionModel(req.body);
        newAction_mongo.save( function(error, data){
          res.send({code:1, newActionData: data})
        })
      }
    });
  }
}

exports.getNocteUsersAchivements = function(req, res){
  var selectedPage = null;
  // console.log('user action history')
  for(var i=0; i<req.user.user.pages.length; i++){
    // console.log(2)
    if(req.user.user.pages[i].id == 550232838377740){
      selectedPage = req.user.user.pages[i];
      next();
      break;
    }
  }

  function next(){
    // console.log('getting game summary')
    if(req.params.uid){
      // console.log('getting achievements')
      // console.log(req.params.uid)
      User.findOne({_id:req.params.uid}, function(err, loggedinUser){
        if(loggedinUser){
          UserAchivements.findOne({userid:loggedinUser._id}).populate('achievements.ach_id', null, 'achievement').exec(function(err,achDoc){
          // UserAchivements.find({userid:loggedinUser._id}, function(err, achDoc){
            // console.log(err + " / " + achDoc)
            if(achDoc && !err){
              res.send({code:1, userachievements: achDoc})
            }
          });
        }
      });
    }
  }
}

exports.getUserActions = function(req,res){

  var rdyToSend = [];

  User.findOne({_id:req.user._id}, function(error, loggedinUser){
    if(loggedinUser){
      var stepCount = 0;

      Actions.find(function(err,actionList) {
        for(var i=0; i<actionList.length;i++){
          var model2look4 = (actionList[i].type=="nocte") ? 'feed' : actionList[i].type
          UserActions.find({'user_id':loggedinUser._id, 'action_type':actionList[i].name}).populate('target_id', null, model2look4 ).populate('action_id', null, 'action').exec(function(err,useractionList){
            finishUp(stepCount, actionList.length, useractionList)
            stepCount++
          });
        }
      })
    }
  });


  function finishUp(typeInd, actionsLength, arrayItem){

    rdyToSend = rdyToSend.concat(arrayItem)
    
    if(typeInd==(actionsLength-1)){
      // console.log(rdyToSend)
      res.send({code:1, userActions: rdyToSend})
    }
  }
}

function getUserGameinfo(req, res, user, callback){
  var rdyToSend = [];
  var userXp = 0;
  var userLevelInfo;
  var userAch;
  var gameLevels;
  var userLevels = [];

  var userXpRdy = false;
  var userAchRdy = false;
  var gameLevelsRdy = false;
  var userLevelInfoRdy = false;

  var uid = (req) ? req.user._id : ((user) ? user._id : '' )
  
  User.findOne({_id:uid}, function(error, loggedinUser){
    console.log('found user')
    if(loggedinUser){
      var stepCount = 0;

      UserActions.find({'user_id':loggedinUser._id}).populate('action_id', null, 'action').exec(function(err,actionList){
        calcXp(actionList)
      });

      UserAchivements.findOne({userid:loggedinUser._id}).populate('achievements.ach_id', null, 'achievement').exec(function(err,achDoc){
        if(achDoc && !err){
          // console.log("************************************")
          // console.log(achDoc)
          userAchRdy = true;
          userAch = achDoc;
          complete();
        }
      });

      GameLevel.find().sort([['max_points', 1]]).exec(function(err, gameLevelsList) {
        gameLevels = gameLevelsList;
        gameLevelsRdy = true;
        calcLevel();
      });
    }
  });

  function calcXp(actions){
    for(var i=0; i<actions.length; i++){
      userXp += actions[i].action_id.value
    }
    userXpRdy = true;
    console.log(userXp)
    calcLevel();
    // complete();
  }

  function calcLevel(){
    if(userXpRdy && gameLevelsRdy){
      for(var i=0;i<gameLevels.length; i++){
        // console.log(gameLevels[i])
        if(gameLevels[i].max_points > userXp){
          userLevelInfo = gameLevels[i];
          userLevels.push(gameLevels[i]);
          userLevelInfoRdy = true;
          break;
        } else {
          userLevels.push(gameLevels[i])
        }
      }
      complete();
    }
  }

  function complete(){
    if(userLevelInfoRdy && userAchRdy){
      console.log('gameinfo complete')
      if(callback){
        console.log('there is a callback')
        var data = {userXp:userXp, userLevel: userLevelInfo, allUserLevels: userLevels}
        callback(data)
      } else {
        res.send({code:1, userXp:userXp, userLevel: userLevelInfo, allUserLevels: userLevels, userAchievements:userAch})
      }
      
    }
  }
}

exports.getUserGameinfo = function(req,res){
  getUserGameinfo(req,res)
  // var rdyToSend = [];
  // var userXp = 0;
  // var userLevelInfo;
  // var userAch;
  // var gameLevels;
  // var userLevels = [];

  // var userXpRdy = false;
  // var userAchRdy = false;
  // var gameLevelsRdy = false;
  // var userLevelInfoRdy = false;

  // User.findOne({_id:req.user._id}, function(error, loggedinUser){
  //   if(loggedinUser){
  //     var stepCount = 0;

  //     UserActions.find({'user_id':loggedinUser._id}).populate('action_id', null, 'action').exec(function(err,actionList){
  //       calcXp(actionList)
  //     });

  //     UserAchivements.findOne({userid:loggedinUser._id}).populate('achievements.ach_id', null, 'achievement').exec(function(err,achDoc){
  //       if(achDoc && !err){
  //         // console.log("************************************")
  //         // console.log(achDoc)
  //         userAchRdy = true;
  //         userAch = achDoc;
  //         complete();
  //       }
  //     });

  //     GameLevel.find().sort([['max_points', 1]]).exec(function(err, gameLevelsList) {
  //       gameLevels = gameLevelsList;
  //       gameLevelsRdy = true;
  //       calcLevel();
  //     });
  //   }
  // });

  // function calcXp(actions){
  //   for(var i=0; i<actions.length; i++){
  //     userXp += actions[i].action_id.value
  //   }
  //   userXpRdy = true;
  //   calcLevel();
  //   // complete();
  // }

  // function calcLevel(){
  //   if(userXpRdy && gameLevelsRdy){
  //     for(var i=0;i<gameLevels.length; i++){
  //       // console.log(gameLevels[i])
  //       if(gameLevels[i].max_points > userXp){
  //         userLevelInfo = gameLevels[i];
  //         userLevels.push(gameLevels[i]);
  //         userLevelInfoRdy = true;
  //         break;
  //       } else {
  //         userLevels.push(gameLevels[i])
  //       }
  //     }
  //     complete();
  //   }
  // }

  // function complete(){
  //   if(userLevelInfoRdy && userAchRdy){
  //     res.send({code:1, userXp:userXp, userLevel: userLevelInfo, allUserLevels: userLevels, userAchievements:userAch})
  //   }
  // }
}

exports.getAllNocteLevels = function(req,res){

  GameLevel.find(function(err, nocteLevels){
    // console.log(nocteLevels)
    if(!err && nocteLevels)
      res.send({code:1, noctelvls: nocteLevels})
  });
}

exports.getNocteUserActions = function(req,res){
  var selectedPage = null;
  // console.log('user action history')
  for(var i=0; i<req.user.user.pages.length; i++){
    // console.log(2)
    if(req.user.user.pages[i].id == 550232838377740){
      selectedPage = req.user.user.pages[i];
      next();
      break;
    } else {
      // if()
    }
  }

  var rdyToSend = [];

  function next(){
    if(req.params.fbid){
      // console.log(req.params.fbid)
      // console.log(req.user.user.id)
      User.findOne({_id:req.params.fbid}, function(error, loggedinUser){
        if(loggedinUser){

          console.log('user here')
          var stepCount = 0;

          Actions.find(function(err,actionList) {
            for(var i=0; i<actionList.length;i++){
              var model2look4 = (actionList[i].type=="nocte") ? 'feed' : actionList[i].type
              UserActions.find({'user_id':loggedinUser._id, 'action_type':actionList[i].name}).populate('target_id', null, model2look4 ).populate('action_id', null, 'action').exec(function(err,actionList){
                finishUp(stepCount, actionList)
                stepCount++
              });
            }
          })
        }
      });
    }
  }

  function finishUp(typeInd, arrayItem){

    var count = 0;
    var finalData = [];

    rdyToSend = rdyToSend.concat(arrayItem)
    // console.log(rdyToSend)
    // console.log(rdyToSend)
    if(typeInd==2){

      for(var i=0; i<rdyToSend.length; i++){

      }
      res.send({code:1, userActions: rdyToSend})
    }
  }

}

exports.getNocteActions = function(req,res){
  var selectedPage = null;
  // console.log(1)
  for(var i=0; i<req.user.user.pages.length; i++){
    // console.log(2)
    if(req.user.user.pages[i].id == 550232838377740){
      selectedPage = req.user.user.pages[i];
      next();
      break;
    }
  }

  function next(){
    
    Actions.find(function(err,actionList) {
      res.send({code:1,nocteActions:actionList});
    })
      
  }
}

exports.suggestedPages = function(req,res){
  SuggestedUser.find(function(err,suggestedusers) {
    res.send({code:1,suggestedlist:suggestedusers});
  });
}

exports.findUser = function(req,res){
  User.findOne({'user.fbid':req.params.fbid},function(err,userData){
    res.send({user:userData.user})
  })
}

exports.deleteSuggestedUser = function(req,res){
  var selectedPage = null;
  // console.log(1)
  for(var i=0; i<req.user.user.pages.length; i++){
    if(req.user.user.pages[i].id == 550232838377740){
      selectedPage = req.user.user.pages[i];
      // console.log(2)
      next();
      break;
    }
  }

  function next(){
    SuggestedUser.findOne({fbid: req.params.fbid}).remove(function(err, data){
      res.send({code:1, pageid:req.params.fbid})
      // console.log('deleted')
      // console.log(data)
    });
  }
}

exports.newClub = function(req,res){
  var selectedPage = null;

  if(req.body.actionType=='add'){
    pageIdAdmin = req.body.nocteId
  } else {
    pageIdAdmin = req.body.clubId
  }

  for(var i=0; i<req.user.user.pages.length; i++){
    if(req.user.user.pages[i].id == pageIdAdmin){
      selectedPage = req.user.user.pages[i];
      next();
      break;
    }
  }

  function next(){
    // console.log('za')
    User.findOne({'user.fbid':req.body.clubId}, function(err,prevUser) {
      if(prevUser){
        User.update({'user.fbid':prevUser.user.fbid}, { 'user.name': req.body.clubname, 'user.description': req.body.clubdesc, 'user.instagram': req.body.clubinstagram}, {}, function(err, numAffected){
            if(numAffected > 0){
              var updatedDj = prevUser.user;
              updatedDj.name = req.body.djname;
              updatedDj.description = req.body.djdesc
              updatedDj.soundcloud = req.body.djsoundcloud
              res.send({code:1, newInfo: updatedDj})
            }
          });
      } else {
        graph
          .setOptions(fbOptions)
          .get(req.body.clubId+"?access_token=" + selectedPage.access_token, function(err, userInfo) {
            
            // console.log(userInfo)
            complete(userInfo);

          }); 

        function complete(fbuserdata){
          var newClubObj = {};
          newClubObj.type = "club"
          newClubObj.user = {};
          newClubObj.user.description = fbuserdata.description
          newClubObj.user.fbid = fbuserdata.id
          newClubObj.user.name =  fbuserdata.name
          newClubObj.user.type = 'club'
          newClubObj.user.username = fbuserdata.username

          var newUser_mongo = new userModel(newClubObj);
          newUser_mongo.save( function(error, data){
            if(!error){
              SuggestedUser.findOne({fbid: req.body.clubId}).remove(function(err, deletedData){
                res.send({code:1, pageid: req.body.clubId, newData: data})
                // res.send({code:1, pageid:req.params.fbid})
              });
              // res.send({code:1, newDj: data})
            }
          });
        }
      }
    });
   }
}

exports.newDj = function(req,res){

  // console.log(req.user.pages)

  var selectedPage = null;

  var pageIdAdmin = null;

  if(req.body.actionType=='add'){
    pageIdAdmin = req.body.nocteId
  } else {
    pageIdAdmin = req.body.djId
  }

  for(var i=0; i<req.user.user.pages.length; i++){
    if(req.user.user.pages[i].id == pageIdAdmin){
      selectedPage = req.user.user.pages[i];
      next();
      break;
    }
  }

  function next(){
      // console.log('za')
      User.findOne({'user.fbid':req.body.djId}, function(err,prevUser) {
        if(prevUser){
          //update user
          User.update({'user.fbid':prevUser.user.fbid}, { 'user.name': req.body.djname, 'user.description': req.body.djdesc, 'user.soundcloud': req.body.djsoundcloud, 'user.instagram': req.body.djinstagram}, {}, function(err, numAffected){
            if(numAffected > 0){
              var updatedDj = prevUser.user;
              updatedDj.name = req.body.djname;
              updatedDj.description = req.body.djdesc
              updatedDj.soundcloud = req.body.djsoundcloud
              res.send({code:1, newInfo: updatedDj})
            }
          });
        } else {
          //get data from facebook
          // console.log(req.user);
          graph
            .setOptions(fbOptions)
            .get(req.body.djId+"?access_token=" + selectedPage.access_token, function(err, userInfo) {
              complete(userInfo);
            }); 

          function complete(fbuserdata){
            var newDjObj = {};
            newDjObj.type = "dj"
            newDjObj.user = {};
            newDjObj.user.description = fbuserdata.description
            newDjObj.user.fbid = fbuserdata.id
            newDjObj.user.firstLogin = false
            newDjObj.user.name =  fbuserdata.name
            newDjObj.user.soundcloud = null
            newDjObj.user.type = 'dj'
            newDjObj.user.username = fbuserdata.username

            var newUser_mongo = new userModel(newDjObj);
            newUser_mongo.save( function(error, data){
              if(!error){
                SuggestedUser.findOne({fbid: req.body.djId}).remove(function(err, deletedCount){
                  res.send({code:1, pageid: req.body.djId, newData: data})
                  // res.send({code:1, pageid:req.params.fbid})
                  // console.log('deleted')
                  // console.log(data)
                });
                // res.send({code:1, newDj: data})
              }
            });

            // res.send(fbuserdata)
            // var newUser_mongo = new userModel(newEventObj);
            // newEvent_mongo.save( function(error, data){
            //   if(!error){
            //     console.log('event added successfully')
            //     // res.send({code:1})
            //   }
            // });
          }
            
        }
      });
    }

}

exports.newEvent = function(req, res){
  var hostPage = null;
  var eventInfo = null;

  // console.log(req.body.addedBy.fbid)

  var selectedPage = null;
  for(var i=0; i<req.user.user.pages.length; i++){
    if(req.user.user.pages[i].id == req.body.addedBy){
      selectedPage = req.user.user.pages[i];
      // console.log(2)
      next();
      break;
    }
  }

  function next(){
    // console.log('za')
    FbFeed.findOne({'details.id':req.body.eventId}, function(err,prevEvent) {

      if(prevEvent){

        // console.log('event var update et. new data:')
        // // console.log(req.body.djs.split(","))
        // console.log(JSON.parse(req.body.eventdjs));

        // console.log(prevEvent)

        FbFeed.update({'details.id':prevEvent.details.id}, { 'details.name': req.body.eventname, 'details.description': req.body.eventdesc, 'details.djs': JSON.parse(req.body.eventdjs)}, {}, function(err, numAffected){
          // console.log(numAffected)
          if(numAffected > 0){
            var updatedEvent = prevEvent.details;
            updatedEvent.name = req.body.eventname
            updatedEvent.description = req.body.eventdesc
            updatedEvent.djs = JSON.parse(req.body.eventdjs)
            res.send({code:1, newInfo: updatedEvent})
          }
        });
      } else {

        // console.log(prevEvent)
        User.findOne({'user.fbid':req.body.addedBy}, function(err, adminUser){
          hostPage = adminUser;
          complete();
        });

        var selectedPage;
        for(var i=0; i<req.user.user.pages.length; i++){
          if(req.user.user.pages[i].id == req.body.addedBy){
            selectedPage = req.user.user.pages[i];
            break;
          }
        }

        graph
          .setOptions(fbOptions)
          .get(req.body.eventId + "?fields=id,owner,cover,name,description,start_time,timezone,venue,privacy,updated_time&access_token=" + selectedPage.access_token, function(err, eventInf) {
            eventInfo = eventInf;
            complete();
          });   

        function complete(){
          if(hostPage != null && eventInfo!=null){
            var newEventObj = {};
            newEventObj.feedtype = "event";
            newEventObj.type = "feed";
            eventInfo.location = hostPage.user.name;
            newEventObj.details = eventInfo;
            // console.log(newEventObj)
            var newEvent_mongo = new feedModel(newEventObj);
            newEvent_mongo.save( function(error, data){
              if(!error){
                // console.log('event added successfully')
                // res.send({code:1})
              }
            });
          }
        }
      }

    });
  }
  
}

exports.getFbEvents = function(req, res){
  var selectedPage;
  for(var i=0; i<req.user.user.pages.length; i++){
    // console.log(i)
    if(req.user.user.pages[i].id == req.params.fbid){
      // console.log("page found")
      // console.log(req.user.user.pages[i]);
      selectedPage = req.user.user.pages[i];
      break;
    }
  }

  var eventIds = [];

  graph
    .setOptions(fbOptions)
    .get(selectedPage.id+"?fields=events.fields(description,cover,end_time,start_time,id,location,name,privacy)&access_token=" + selectedPage.access_token, function(err, events) {
      
      // console.log('zaaa')
      // console.log(events)

      if(events.events){
        for(var i=0; i<events.events.data.length;i++){
          eventIds.push(events.events.data[i].id)
        }

        FbFeed.find({'details.id':{$in: eventIds} },function(err,docs){
          // if(events.events.data.length>0){
          for(var i=0; i<events.events.data.length;i++){
            for(var j=0; j<docs.length; j++){
              if(events.events.data[i].id == docs[j].details.id){
                events.events.data[i].nocteEvent = true;
                events.events.data[i].nocteData = docs[j].details
              }
            }
            res.send(events.events.data)
          }
          // } else {
            // events.events.data = ''
            // res.send(events.events.data)
          // }
        });
      } else {
        // events.events.data = ''
        res.send('')
      }
      // console.log(eventIds)
      // res.send(events.events.data)
    });   
}

exports.suggestNewUser = function(req, res){

  //check if the user is already present
  User.findOne({'user.fbid':req.params.fbid}, function(err, prevUser){
    if(prevUser){
      // console.log('already present')
      res.send({code:3})
      //already a member of nocte
    } else {
      //check if the user has been suggested before.
      SuggestedUser.findOne({fbid:req.params.fbid}, function(err, preSuggested){
        // console.log('err: ' + err)
        if(preSuggested){ 
          // console.log('already suggested')
          res.send({code:2})
          //has been suggested before
        } else {
          var addedByUser = {};
          addedByUser.fbid = req.user.user.id;
          addedByUser.name = req.user.user.name;
          var newSuggestedUser = new suggestedUserModel({fbid:req.params.fbid, addedBy:addedByUser});
          newSuggestedUser.save( function(error, data){
            if(!error){
              // console.log('suggestion successful')
              res.send({code:1})
            }
          });
        }
      })
    }
  });
}

exports.preRegUser = function(req, res){
  EarlyUser.findOne({email:req.body.email}, function(err, earlyU){
    if(earlyU){
      res.send({code:1})
    } else if(!err){
      var newEarlyUser = new earlyUserModel({email:req.body.email});
      newEarlyUser.save( function(error, data){
        if(!error){
          res.send({code:1})
        }
      });
    }
  });
}
exports.getLikes = function(req, res){
  UserActions.find({action_type:'like'}).populate('target_id', null, 'feed').exec(function(err, actions){
  })
}

exports.findOrCreateUser = function(profile, done, newData){

  // console.log("****** ANANELER *****")
  // console.log(profile)
  User.findOne({'user.id':profile.id}, function(err, user){
    // console.log("****** ANANELER *****")
    // console.log(user)
    if(user){
      var userProfile = user
      userProfile.user.pages = newData.data
      done(null, userProfile)
    } else {
      var userProfile = profile._json;

      userProfile.pages = (newData.data.length !==0) ? newData.data : false;
      
      userProfile.firstLogin = true;
      var newUser = new userModel({user:userProfile, type:'member'});
      newUser.save( function(error, data){
        if(!error){
          addAction('register',null, null, data, null)
          done(null, data);
        }
      });
    }
  })
}
 
exports.feedList = function(req, res) {
  FbFeed.find(function(err, feeds) {
    // console.log(feeds)
    res.send(feeds);
  });
}

exports.djEvents = function(req,res){
  // console.log(req.params.djId)
  FbFeed.find({'details.djs.nocteId': req.params.djId}, function(err, feeds) {
    // console.log(err + " / " + feeds)
    res.send(feeds);
  });
}

exports.clubFeedList = function(req, res){

  // console.log(req.params.clubname)
  FbFeed.find({'details.location': req.params.clubname}, function(err, feeds) {
    res.send(feeds);
  });
}

exports.clubs = function(req,res) {
  User.find({type:'club'}, function(err, clubs) {
    res.send(clubs);
  });
}

exports.djs = function(req,res) {
  if(req.body.djs){
    User.find({'user.fbid':{$in:req.body.djs.split(',')}}, function(err, noctedjs){
      res.send({code:1, noctedjs: noctedjs})
    });
  } else {
    User.find({type:'dj'}, function(err, djs) {
      res.send(djs);
    });
  }
}

exports.djDetails = function(req, res) {
  User.find({'user.username': req.params.djName}, function(err, djDetails) {
    res.send(djDetails);
  });
}

exports.feedDetails = function(req, res) {
  FbFeed.find({_id: req.params.feedid}, function(err, eventDetails) {
    // console.log(eventDetails)
    res.send(eventDetails);
  });
}

exports.clubDetails = function(req, res) {
  User.find({'user.name': req.params.clubname}, function(err, clubDetails) {
    res.send(clubDetails);
  });
}

function addAction(actionName, actionTargetId, res, user, parentid){
  
  var actionIns = new Object();

  Actions.findOne({name: actionName}, function(err, action) {
    var targetid;

    switch(action.type){
      case 'user':
        targetid = actionTargetId;
        break;
      case 'feed':
        targetid = actionTargetId;
        break;
      default:
        targetid = action._id  //dirty fix
        break;
    }

    if(action.name == "feedback"){
      Feedback.find({userEmail: user.user.email}, function(err, userfeedbacks){
        // console.log(userfeedbacks)
        checkAchievement(action.name, res, user, '', userfeedbacks)
      })
    } else {

      var userActionObject;

      if(actionName == "musicplay"){
        console.log('musicAch ' + parentid)
        userActionObject = {user_id: user._id, action_type: action.name, action_id:action._id ,target_id: targetid, target_parentid: parentid};
      } else {
        userActionObject = {user_id: user._id, action_type: action.name, action_id:action._id ,target_id: targetid};
      }
      // var userActionObject = {user_id: user._id, action_type: action.name, action_id:action._id ,target_id: targetid};
      UserActions.findOne(userActionObject, function(err, userActionDoc) {
        if(!err && userActionDoc){
          // console.log('action already there')
        } else if (!err && !userActionDoc) {
          userActionObject.date = new Date();
          var newAction = new actionModel(userActionObject);
          newAction.save(function(err, data){
            if(!err) {

              var achievementsRdy = false;
              var achievementsObj = {};
              var gameinfoRdy = false;
              var gameinfoObj = {};
              var lastActionRdy = false;
              var lastactionObj = {};

              function next(dataobj){
                if(dataobj.achRdy){
                  achievementsRdy = true;
                  achievementsObj = dataobj
                } else if (dataobj.userXp){
                  gameinfoRdy = true;
                  gameinfoObj = dataobj
                } else if (dataobj.action){
                  lastActionRdy = true;
                  lastactionObj = dataobj
                }
                if(achievementsRdy && gameinfoRdy && lastActionRdy){
                  res.send({code:1, achinfo: achievementsObj, gameinfo: gameinfoObj, lastaction: lastactionObj})
                }

              }

              checkAchievement(action.name, res, user, '', '', next);
              getUserGameinfo('', res, user, next)

              UserActions.findOne(userActionObject).populate('target_id', null, (action.type=='nocte') ? '': action.type ).populate('action_id', null,'action').exec(function(err, lastAction){
                if(lastAction)
                  next({action: lastAction})
              });
            }
          })
        }
      });
    }
  });
}

function checkAchievement(actionName, res, user, lastAction, useractions, callback){

  if(actionName == "feedback"){
    console.log(useractions)
    next(useractions)
  } else {
    UserActions.find({user_id: user._id, action_type: actionName}, function(err, userActions){
      if(!err && userActions){
        next(userActions)
      }
    });
  }
    

  function next(useractions){
    console.log(useractions)
    //count user actions
    // var count = 0;
    // for(var i=0; i<useractions.length;i++){
    //   if(useractions[i].name==actionName){
    //     count++
    //   }
    // }

    // console.log('count is:')
    // console.log(count)

    Achivements.find({action:actionName, repeat: {$lte: useractions.length}}, function(err, achievements) {
      
      var achievementData = [];
      //check if theres an achievement
      if(achievements[0]){

        console.log('theres an achievement')
        var dupeAchievement = false;

        // console.log()
        UserAchivements.find({userid:user._id}, function(err, achDoc){
          achievements[0].date = new Date();
          //Check if the user_achievements doc is present
          if(achDoc[0]){
            //check if the user already has this achievement
            UserAchivements.findOne({userid: user._id, 'achievements.ach_id': achievements[0]._id}, function(err, prevAction) {
              if(prevAction){
                dupeAchievement = true;
              } else {
                achDoc[0].achievements.push({ach_id : achievements[0]._id, date:achievements[0].date });
                achDoc[0].save(function(error, data){
                });
              }
              achievementData = (!dupeAchievement) ? {ach_id: achievements[0]} : null;
              if(res){
                if(callback){
                  var data = {achRdy: true, achievement: achievementData}
                  callback(data)   
                } else {
                  res.send({code:'1', user:user, lastAction:lastAction, useractions: useractions, achievement: achievementData})
                }
              }

            });

          } else {
            var newUserAchievemenModel = new userAchievementModel({userid: user._id});
            newUserAchievemenModel.achievements.push({ach_id : achievements[0]._id, date:achievements[0].date })
            newUserAchievemenModel.save(function(error, data){
              if(!error){
              }
            });
            achievementData = (!dupeAchievement) ? {ach_id: achievements[0]} : null;
            if(res){
              if(callback){
                var data = {achRdy: true, achievement: achievementData}
                callback(data)  
              } else {
                res.send({code:'1', user:user, lastAction:lastAction, useractions: useractions, achievement: achievementData})
              }
            }

          }
        })
      } else {
        console.log('there is not an achievement')
        achievementData = null;
        if(res){
          // getUserGameInfo()
          if(callback){
            var data = {achRdy: true, achievement: achievementData}
            callback(data) 
          } else {
            res.send({code:'1', user:user, lastAction:lastAction, useractions: useractions, achievement: achievementData})
          }
        }

      }

    });
  }
}

exports.deleteUserAction = function(req,res){
  var target = null;
  if(req.params.targetType){
    target = [];
    target.type = req.params.targetType;
    target.id = req.params.targetId
  }
  User.findOne({'user.id':req.user.user.id}, function(error, loggedinUser){
    UserActions.findOne({user_id: loggedinUser._id, target_id : target.id}).remove(function(err, data){
      Actions.findOne({name:req.params.actionType}, function(err, nocteAction){
        res.send({code:'1', user:loggedinUser, lastAction:nocteAction})
      });
      // console.log(data)
      // res.send({code:'1', user:loggedinUser})
    })
  });
}
exports.newUserAction = function(req,res){
  var target = null;
  console.log("************")
  User.findOne({'user.id':req.user.user.id}, function(error, loggedinUser){
    addAction(req.body.actionName, req.body.targetId, res, loggedinUser, req.body.parentid)
  });
}

exports.getUserLikes = function(user, res){
  User.findOne({'user.id':user.user.id}, function(error, loggedinUser){
    UserActions.find({user_id: loggedinUser._id, action_type: 'like'}).populate('target_id', null,'feed').exec(function(err, actions){
      res.send({actions:actions})
    });

  });
}

exports.getUserFollows = function(user, res){
  User.findOne({'user.id':user.user.id}, function(error, loggedinUser){
    UserActions.find({user_id: loggedinUser._id, action_type: 'follow'}).populate('target_id', null, 'user').exec(function(err, actions){
      res.send({actions:actions})
    });

  });
}

exports.getUserAction = function(user, res, sendUser){
  User.findOne({'user.id':user.user.id}, function(error, loggedinUser){
    
    var noctePgsRdy = false;
    var suggestedPgsRdy = false;
    var actionsRdy = false;
    var achsRdy = false;

    var useractions, userachievements
    var allpages = [];

    // console.log("********")
    // console.log(user.user.pages)
    if(user.user.pages[0]){

      //now create a new motherfucking array of pages
      //cuz WE DONT WANNA SEND ACCESS TOKENS TO THE MOTHERFUCKING CLIENT

      var pageIds = [];

      // console.log('za')
      
      for(var i=0; i<user.user.pages.length;i++){
        pageIds.push(user.user.pages[i].id)
        allpages[i] = {}
        allpages[i].id = user.user.pages[i].id;
        allpages[i].name = user.user.pages[i].name;
        allpages[i].suggested = false;
      }
      User.find({'user.fbid':{$in: pageIds} },function(err,docs){
      
        for(var i=0; i<docs.length; i++){
          for(var j=0; j<allpages.length;j++){
            if(allpages[j].id == docs[i].user.fbid){
              allpages[j].noctePage = true;
            } else if(!allpages[j].noctePage) {
              allpages[j].noctePage = false;
            }
          }
          if (i==(docs.length-1)){
            noctePgsRdy = true;
            // console.log(1)
            complete();
          }
        }

        SuggestedUser.find({fbid:{$in:pageIds}}, function(err, preSuggested){
          if(preSuggested.length>0){
            for(var k=0; k<preSuggested.length; k++){
              for(var l=0; l<allpages.length;l++){
                if(allpages[l].id == preSuggested[k].fbid){
                  allpages[l].suggested = true;
                } else if(!allpages[l].suggested) {
                  allpages[l].suggested = false;
                }
              }
              if(k==(preSuggested.length-1)){
                suggestedPgsRdy = true;
                // console.log(2)
                complete();
              }
            }
          } else {
            for(var l=0; l<allpages.length;l++){
              allpages[l].suggested = false;
            }
            suggestedPgsRdy = true;
            // console.log(2)
            complete();
          }
        });

        loggedinUser.user.noctepages = docs;
        loggedinUser.user.allpages = allpages;
      })
    } else {
      suggestedPgsRdy = true;
      noctePgsRdy = true;
      complete();
    }

    if(loggedinUser.user.firstLogin){
      User.update({'user.id':user.user.id}, { 'user.firstLogin': false}, {}, function(err, numAffected){});
    }

    // UserActions.find({user_id: loggedinUser._id}, function(err,actions){
    //   if(!err){
    //     actionsRdy = true;
    //     useractions = actions;
    //     complete();
    //   }
    // })  
    // UserAchivements.find({userid:loggedinUser._id}, function(err, achDoc){
    //   if(!err){
    //     achsRdy = true;
    //     if(achDoc[0])
    //       userachievements = achDoc[0].achievements
    //     else 
    //       userachievements = null;
    //     complete();
    //   }
    // });

    function complete(){
      if(suggestedPgsRdy && noctePgsRdy){
        // console.log('good to go')
        // console.log(loggedinUser)
        res.send({user: loggedinUser});
      } else {
        // console.log('waiting for everthing to get rdy')
      }
      // console.log('zaaa')
    }

  })
}
